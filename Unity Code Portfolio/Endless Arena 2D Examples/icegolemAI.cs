﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class icegolemAI : MonoBehaviour
{
    [SerializeField]
    private float _maxLife = 250;
    public float life;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _followRange;
    [SerializeField]
    private float _attackRange;

    private Animator anim;
    private Transform target;
    public int value;
    public float attackRate;
    public bool canAttack = true;
    public Vector3 direction;
    public LayerMask whatIsPlayer;
    public GameObject[] attackPos;
    public bool canMove;
    public event System.Action<float> OnHealtPctChanged = delegate { };
    public GameObject[] buffs;
    private Rigidbody2D body;
    private Vector3 damageDir;

    [SerializeField]
    private SpawnManager spawner;
    public GameManager gameManager;
    public GameObject particle;
    private AudioSource _audio;
    public AudioClip hit;
    public AudioClip dead;

    private void Start()
    {
        life = _maxLife;
        anim = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        canAttack = true;
        canMove = true;
        spawner = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
        value = Random.Range(8, 21);
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        _audio = GetComponent<AudioSource>();


    }
    private void Update()
    {
        FollowMovement();
        StatusControl();
    }

    private void FollowMovement()
    {
        if (target != null)
        {
            if (Vector2.Distance(transform.position, target.position) > _followRange)
            {
                if (canMove == true)
                {
                    transform.position = Vector2.MoveTowards(transform.position, target.position, _speed * Time.deltaTime);
                }
            }
            else
            {
                if (canAttack == true)
                {
                    StartCoroutine(AttackRoutine());//Attack
                }

            }
            direction = (target.position - transform.position).normalized;
            anim.SetFloat("dirX", direction.x);
            anim.SetFloat("dirY", direction.y);
        }
    }

    public void GiveDamage(int direction)
    {
        Collider2D enemiesToDamage = Physics2D.OverlapCircle(attackPos[direction].GetComponent<Transform>().position, _attackRange, whatIsPlayer);
        if (enemiesToDamage != null)
        {
            enemiesToDamage.SendMessage("TakeDamage", 15f);
            enemiesToDamage.SendMessage("TakeDamageLoc", transform.position);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos[2].GetComponent<Transform>().position, _attackRange);
    }

    private IEnumerator AttackRoutine()
    {
        canMove = false;
        anim.SetBool("isAttacking", true);
        canAttack = false;
        yield return new WaitForSeconds(attackRate);
        canAttack = true;
        anim.SetBool("isAttacking", false);
        canMove = true;
    }
    public void TakeDamage(int dmg)
    {
        life -= dmg;
        float currentHealthPct = (float)life / (float)_maxLife;
        OnHealtPctChanged(currentHealthPct);
        StartCoroutine(DamageTakenIndicator());
        _audio.PlayOneShot(hit);
        //splash effect here
    }
    public void TakeDamageLoc(Vector3 dir)
    {
        DamageDirection(dir);
    }
    private void DamageDirection(Vector3 lc)
    {
        damageDir = (lc - transform.position).normalized;
    }

    private void SpawnBuffs()
    {
        int x = Random.Range(0, 2);
        float chanceForBuff = Random.Range(0f, 1f);
        if (chanceForBuff < 0.15)
        {
            Instantiate(buffs[x], transform.position, Quaternion.identity);
        }
    }
    private void StatusControl()
    {
        if (life <= 0)
        {
            AudioSource.PlayClipAtPoint(dead, this.gameObject.transform.position);
            //Debug.Log("Object Destroyed");
            Destroy(gameObject);
            spawner.enemyCounter--;
            //game respawn burada
            spawner.EnemyCheck();
            gameManager.IncreaseGold(value);
            SpawnBuffs();
            Instantiate(particle, transform.position, Quaternion.identity);
        }
    }
    private IEnumerator DamageTakenIndicator()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().color = Color.white;
    }
}
