﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shroom01AI : MonoBehaviour
{
    [SerializeField]
    private float _maxLife;
    public float life;

    [SerializeField]
    private float _speed;

    public float attackRate;
    public float attackRange;
    public bool isAttacking = false;

    public GameObject crack;

    private Vector3 directionNorm;
    private Vector3 damageDir;

    public Transform target;

    public Animator anim;
    private Rigidbody2D body;

    public bool _knockbackStatus;
    public GameObject[] buffs;
    public SpawnManager spawner;
    public GameManager gameManager;
    public Vector3 attackOffset;

    private int value;

    public GameObject splash;

    private AudioSource _audio;
    public AudioClip hit;
    public AudioClip dead;

    public event System.Action<float> OnHealtPctChanged = delegate { };
    private void Start()
    {
        life = _maxLife;
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody2D>();
        spawner = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        value = Random.Range(8, 21);
        crack.GetComponent<ShroomCrack>().Damage = 10f;
        _audio = GetComponent<AudioSource>();
    }
    private void Update()
    {
        Movement();
        StatusControl();
    }

    private void Movement()
    {
        if (isAttacking==false)
        {
            if (Vector2.Distance(transform.position, target.position) > attackRange)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, _speed * Time.deltaTime);
            }
            else
            {
                StartCoroutine(AttackSequence());
            }
        }

        directionNorm = (target.position - transform.position).normalized;
        anim.SetFloat("dirX",directionNorm.x);
        anim.SetFloat("dirY", directionNorm.y);
    }
    public void Attack()
    {
        Instantiate(crack,target.position-attackOffset,Quaternion.identity);
    }
    private IEnumerator AttackSequence()
    {
        anim.SetBool("isAttacking",true);
        isAttacking = true;
        yield return new WaitForSeconds(attackRate);
        isAttacking = false;
        anim.SetBool("isAttacking", false);
    }
    public void TakeDamage(int dmg)
    {
        life -= dmg;
        float currentHealthPct = (float)life / (float)_maxLife;
        OnHealtPctChanged(currentHealthPct);
        _audio.PlayOneShot(hit);
    }
    public void TakeDamageLoc(Vector3 dir)
    {
        DamageDirection(dir);
        StartCoroutine(Knockback());
    }
    private void DamageDirection(Vector3 lc)
    {
        damageDir = (lc - transform.position).normalized;
    }
    private IEnumerator Knockback()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
        _knockbackStatus = true;
        //body.isKinematic = fal;
        body.AddForce(damageDir * -10f, ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.2f);
        //body.isKinematic = false;
        body.velocity = Vector3.zero;
        _knockbackStatus = false;
        GetComponent<SpriteRenderer>().color = Color.white;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            body.velocity = Vector3.zero;
            Debug.Log("Collision vector3 zero");
            collision.rigidbody.velocity = Vector3.zero;
        }
    }
    private void SpawnBuffs()
    {
        int x = Random.Range(0, 2);
        float chanceForBuff = Random.Range(0f, 1f);
        if (chanceForBuff < 0.15)
        {
            Instantiate(buffs[x], transform.position, Quaternion.identity);
        }
    }
    private void StatusControl()
    {
        if (life <= 0)
        {
            AudioSource.PlayClipAtPoint(dead, this.gameObject.transform.position);
            Destroy(gameObject);
            spawner.enemyCounter--;
            //game respawn burada
            spawner.EnemyCheck();
            gameManager.IncreaseGold(value);
            SpawnBuffs();
            Instantiate(splash, transform.position, Quaternion.identity);

        }
        if (_knockbackStatus == false)
        {
            body.velocity = Vector3.zero;
        }
    }
    public void ChangeAttackAnim()
    {
        anim.SetBool("isAttacking",false);
    }
}
