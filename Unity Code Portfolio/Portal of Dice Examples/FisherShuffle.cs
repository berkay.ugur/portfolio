﻿using System.Collections.Generic;
using System;

static class FisherShuffle 
{
    public static Random rnd=new Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int i = list.Count;
        while (i>1)
        {
            i--;
            int k = rnd.Next(i + 1);
            T value = list[k];
            list[k] = list[i];
            list[i] = value;
        }
    }
}
