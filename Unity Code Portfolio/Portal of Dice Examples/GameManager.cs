﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        [SerializeField]private string _gameState;
        private float _gold;
        private float _runGold;

        public PowerUpController powerUpController;

        public string GameState
        {
            get => _gameState;
            set => _gameState = value;
        }

        public float Gold
        {
            get => _gold;
            set => _gold = value;
        }

        public float RunGold
        {
            get => _runGold;
            set => _runGold = value;
        }

        private void Awake()
        {
            #region Singleton
            
            if (Instance==null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(gameObject);

            #endregion

            _gold = 100;
            _runGold = 0;
            powerUpController = GetComponent<PowerUpController>();
        }

        private void Start()
        {
            GameState = "MainMenu";
        }

        public void ChangeScene(string sceneName)
        {
            SceneManager.LoadSceneAsync(sceneName);
            GameState = sceneName;
        }
    }
}
