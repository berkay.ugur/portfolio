﻿using System;
using System.Collections.Generic;
using Battle;
using Cards;
using Core.Data;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core
{
    public class DeckManager : MonoBehaviour
    {
        public static DeckManager instance;
        public List<Card> curOnDeck;
        public List<Card> curOnHand;
        public List<Card> usedCards;
        public List<Card> initialDeck;
        public GameObject cardLocation;
        public GameObject directCardTemplate;
        public GameObject indirectCardTemplate;
        public GameObject battleManager;
        public Vector3 offsetVal;
        private void Awake()
        {
            #region Singleton

            if (instance==null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(gameObject);
            
            #endregion
            SceneManager.sceneLoaded += OnSceneWasSwitched;
            //Sub OnBattleStageChanged from room thing.
            curOnDeck=new List<Card>(initialDeck);
            curOnDeck.Shuffle();
            //Do shuffle here
        }

        private void OnSceneWasSwitched(Scene arg0, LoadSceneMode arg1)
        {
            battleManager=GameObject.FindGameObjectWithTag("BattleManager");
            if (battleManager!=null)
            {
                cardLocation = battleManager.GetComponent<BattleController>().cardLocation;
                battleManager.GetComponent<BattleController>().OnBattleStageChanged += HandleBattleStageChange;
            }
        }


        private void HandleBattleStageChange(BattleStage stage)
        {
            if (stage==BattleStage.Rolling)
            {
                DrawCards(4);
            }
            else if (stage==BattleStage.End)
            {
                SendAllCardsYoUsed();
                ClearCards();
            }
        }

        private void DrawCards(int dValue)
        {
            GameObject drawCard;
            var pos = cardLocation.transform.position-new Vector3(6f,0,0);
            //we have 5 card on deck and we are drawing 4 card
            if (dValue<=curOnDeck.Count)
            {
                for (int i = 0; i < dValue; i++)
                {
                    InstantiateWithCardType(curOnDeck[0],pos);
                    curOnHand.Add(curOnDeck[0]);
                    curOnDeck.Remove(curOnDeck[0]);
                    //curOnDeck.RemoveAt(0);
                    pos += offsetVal;
                }
            }
            else
            {
                var drawN = curOnDeck.Count;
                for (int i = 0; i < drawN; i++)
                {
                    InstantiateWithCardType(curOnDeck[0],pos);
                    curOnHand.Add(curOnDeck[0]);
                    curOnDeck.Remove(curOnDeck[0]);
                    //curOnDeck.RemoveAt(0);
                    pos += offsetVal;
                }
                //Take usedCardTo deck and shuffle
                PutUsedCardsToDeck();
                curOnDeck.Shuffle();
                drawN = dValue - drawN;
                for (int i = 0; i < drawN; i++)
                {
                    InstantiateWithCardType(curOnDeck[0],pos);
                    curOnHand.Add(curOnDeck[0]);
                    curOnDeck.Remove(curOnDeck[0]);
                    pos += offsetVal;
                }
            }
        }
        public void SendCardToUsed(GameObject card)
        {
            foreach (var c in curOnHand)
            {
                if (card.GetComponent<InitializeCard>().card==c)
                {
                    usedCards.Add(c);
                    curOnHand.Remove(c);
                    break;
                }
            }
        }
        private void SendAllCardsYoUsed()
        {
            usedCards.AddRange(curOnHand);
            curOnHand.Clear();
        }

        private void PutUsedCardsToDeck()
        {
            curOnDeck=new List<Card>(usedCards);
            usedCards.Clear();
        }

        private void ClearCards()
        {
            var obj = GameObject.FindGameObjectsWithTag("Card");
            foreach (var o in obj)
            {
                Destroy(o.gameObject);
            }
        }

        private void InstantiateWithCardType(Card card,Vector3 pos)
        {
            GameObject drawCard;
            if (card is DirectCard)
            {
                drawCard=Instantiate(directCardTemplate,pos,quaternion.identity, cardLocation.transform);
                drawCard.GetComponent<InitializeCard>().card = curOnDeck[0];
            }
            else if (card is IndirectCard)
            {
                drawCard=Instantiate(indirectCardTemplate,pos,quaternion.identity, cardLocation.transform);
                drawCard.GetComponent<InitializeCard>().card = curOnDeck[0];
            }
        }

        public void AddCardToDeck(Card card)
        {
            curOnDeck.Add(card);
            Debug.Log($"{card.name} added to deck.");
        }

    }
}
