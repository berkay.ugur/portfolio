﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Core
{
    /// <summary>
    /// Main Entry class.
    /// Contains randomization data for objects.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Entry<T>
    {
        /// <summary>
        /// Stores the accumulated weight for item.
        /// </summary>
        public float accWeight;
        /// <summary>
        /// Stores the item
        /// </summary>
        public T item;
        /// <summary>
        /// Maximum value that the item can be selected
        /// </summary>
        public int max;
        /// <summary>
        /// Minimum value that the item can be selected
        /// </summary>
        public int min;
        /// <summary>
        /// Item's weight for randomization
        /// </summary>
        public float weight;

        /// <summary>
        /// Constructor for items with Dynamic Weight system.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="weight"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public Entry(T item, float weight, int min, int max)
        {
            this.item = item;
            this.weight = weight;
            this.min = min;
            this.max = max;
        }
        /// <summary>
        /// Constructor for Random without dynamic weight system
        /// </summary>
        /// <param name="item"></param>
        public Entry(T item)
        {
            this.item = item;
        }
    }

    public class DWeightedRandom<T>
    {
        private List<Entry<T>> _entries = new List<Entry<T>>();
        private List<Entry<T>> _minEntries = new List<Entry<T>>();
        private Random rand = new Random();
        private float accWeight;
        private int minItemToSelect;

        public void AddEntry(T item, float weight, int min, int max)
        {
            var entry = new Entry<T>(item, weight, min, max);
            accWeight += weight;
            entry.accWeight = accWeight;
            _entries.Add(entry);
            minItemToSelect += min;
        }

        public void AddEntry(T item,float weight)
        {
            var entry=new Entry<T>(item);
            accWeight += weight;
            entry.accWeight = accWeight;
            _entries.Add(entry);
        }

        public T GetRandom()
        {
            var r = rand.NextDouble() * accWeight;
            foreach (var entry in _entries)
                if (entry.accWeight >= r)
                {
                    return entry.item;
                }

            return default;
        }

        public T GetRandom(int remaining)
        {
            //Debug.Log($"<color=yellow>R {remaining}</color>");
            //Debug.Log($"<color=yellow>M {minItemToSelect}</color>");
            var r = rand.NextDouble() * accWeight;
            Entry<T> selectedEntry;

            if (remaining <= minItemToSelect && minItemToSelect != 0)
            {
                //Select remaining items random
                //create new list for remaining
                //search _entries for finding min entries
                FindEntriesWithMin();
                return GetRandomFromMin();
            }

            foreach (var entry in _entries)
                if (entry.accWeight >= r)
                {
                    // Debug.Log("<color=blue> Entry[i]:</color>");
                    // Debug.Log($"Item:{entry.item.ToString()}");
                    // Debug.Log($"Weight:{entry.weight}");
                    // Debug.Log($"Min,Max:{entry.min},{entry.max}");
                    if (entry.min > 0) minItemToSelect--;
                    entry.min--;
                    entry.max--;
                    float weightChange = 0;
                    if (entry.max == 0)
                    {
                        weightChange = entry.weight;
                        selectedEntry = entry;
                        RemoveEntry(entry, _entries);
                        RearrangeWeight(weightChange);
                    }
                    else
                    {
                        weightChange = 1f;
                        selectedEntry = entry;
                        RearrangeWeight(entry, 1f);
                        // Debug.Log("<color=blue> Entry[i]:</color>");
                        // Debug.Log($"Item:{entry.item.ToString()}");
                        // Debug.Log($"Weight:{entry.weight}");
                        // Debug.Log($"Min,Max:{entry.min},{entry.max}");
                        // Debug.Log("<color=red>Returned entry</color>");
                        // Debug.Log($"Item:{selectedEntry.item.ToString()}");
                        // Debug.Log($"Weight:{selectedEntry.weight}");
                        // Debug.Log($"Min,Max:{selectedEntry.min},{selectedEntry.max}");
                    }

                    return selectedEntry.item;
                }

            return default;
        }

        private void RemoveEntry(Entry<T> entry, List<Entry<T>> list)
        {
            foreach (var e in list)
                if (Equals(e.item, entry.item))
                {
                    list.Remove(e);
                    //Debug.Log($"<color=red>Entry {e.item.ToString()} deleted.</color>");
                    return;
                }
        }

        private void RearrangeWeight(List<Entry<T>> list)
        {
            accWeight = 0f;
            foreach (var entry in list)
            {
                accWeight += entry.weight;
                entry.accWeight = accWeight;
            }
        }

        private void RearrangeWeight(float value)
        {
            //After Remove an Entry
            accWeight = 0f;
            var addedWeight = value / _entries.Count;
            foreach (var entry in _entries)
            {
                entry.weight += addedWeight;
                accWeight += entry.weight;
                entry.accWeight = accWeight;
                // Debug.Log("<color=green>Rearranging entries</color>");
                // Debug.Log($"Item:{entry.item.ToString()}");
                // Debug.Log($"Weight:{entry.weight}");
                // Debug.Log($"Min,Max:{entry.min},{entry.max}");
                // Debug.Log($"AccWeight:{entry.accWeight}");
            }

            //Debug.Log($"Accumulated Weight after removing:{accWeight}, added:{addedWeight}");
        }

        private void RearrangeWeight(Entry<T> entry, float value)
        {
            accWeight = 0f;
            var addedWeight = value / (_entries.Count - 1);
            foreach (var e in _entries)
            {
                if (Equals(e.item, entry.item))
                {
                    entry.weight -= value;
                    accWeight += entry.weight;
                    entry.accWeight = accWeight;
                    continue;
                }

                e.weight += addedWeight;
                accWeight += e.weight;
                e.accWeight = accWeight;
                // Debug.Log("<color=red>_entries[i] Entry:</color>");
                // Debug.Log($"Item:{e.item.ToString()}");
                // Debug.Log($"Weight:{e.weight}");
                // Debug.Log($"Min,Max:{e.min},{e.max}");
            }

            //Debug.Log($"Accumulated Weight after selecting:{accWeight}, added:{addedWeight}");
        }

        private void FindEntriesWithMin()
        {
            foreach (var entry in _entries)
                if (entry.min > 0)
                    _minEntries.Add(entry);
        }

        private T GetRandomFromMin()
        {
            var r = rand.NextDouble() * accWeight;
            Entry<T> selectedEntry;
            foreach (var entry in _minEntries)
                if (entry.accWeight >= r)
                {
                    if (entry.min > 1)
                    {
                        minItemToSelect--;
                        entry.min--;
                    }
                    else
                    {
                        selectedEntry = entry;
                        RemoveEntry(entry, _minEntries);
                        //rearrange here
                        RearrangeWeight(_minEntries);
                        return selectedEntry.item;
                    }

                    return entry.item;
                }

            return default;
        }
    }
}