﻿using System;
using System.Collections.Generic;
using Core;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Dungeon
{
    public class RoomController : MonoBehaviour
    {
        public Sprite testSprite;
        public List<Sprite> roomIcons=new List<Sprite>();
        DWeightedRandom<RoomType> mRoom=new DWeightedRandom<RoomType>();
        DWeightedRandom<RoomType> sRoom=new DWeightedRandom<RoomType>();

        public int remainingMainRoom;
        public int remainingSideRoom;
        //sprite 0 treasure
        //1 battle
        //2 boss
        //3 shop
        //4 shrine
        //5 rest
        //TODO:Need a good randomizer
        //TODO:Need enemylist and bosslist for setting battle rooms.
        //0-Battle
        //1-Shop
        //2-Rest
        //3-Treasure

        //TODO:Need to assing roomtype battle for first and last room 

        private void Awake()
        {
            mRoom.AddEntry(RoomType.Battle,5,2,4);
            mRoom.AddEntry(RoomType.Shop,1,0,1);
            mRoom.AddEntry(RoomType.Treasure,2.5f,0,0);
            mRoom.AddEntry(RoomType.Rest,1,0,1);
            
            sRoom.AddEntry(RoomType.Battle,0.75f,0,1);
            sRoom.AddEntry(RoomType.Shop,1.75f,0,1);
            sRoom.AddEntry(RoomType.Treasure,5,0,0);
            sRoom.AddEntry(RoomType.Rest,0.75f,0,1);
        }

        private RoomType RandomizeRoom(GameObject spawnedRoom,bool sideroom)
        {
            int remainingRoom;
            RoomType randomizedRoom;
            if (sideroom)
            {
                remainingRoom = remainingSideRoom;
                randomizedRoom = sRoom.GetRandom(remainingRoom);
            }
            else
            {
                remainingRoom = remainingMainRoom;
                randomizedRoom = mRoom.GetRandom(remainingRoom);
            }
            //var weightedRoom=mRoom.GetRandom(remainingRoom);
            switch (randomizedRoom)
            {
                case RoomType.Battle:
                    spawnedRoom.AddComponent<BattleRoom>();
                    FindChildGameObject(spawnedRoom).GetComponent<SpriteRenderer>().sprite = roomIcons[1];
                    return RoomType.Battle;
                case RoomType.Shop:
                    spawnedRoom.AddComponent<ShopRoom>();
                    FindChildGameObject(spawnedRoom).GetComponent<SpriteRenderer>().sprite = roomIcons[3];
                    return RoomType.Shop;
                case RoomType.Treasure:
                    spawnedRoom.AddComponent<TreasureRoom>();
                    FindChildGameObject(spawnedRoom).GetComponent<SpriteRenderer>().sprite = roomIcons[0];
                    return RoomType.Treasure;
                case RoomType.Rest:
                    spawnedRoom.AddComponent<RestRoom>();
                    FindChildGameObject(spawnedRoom).GetComponent<SpriteRenderer>().sprite = roomIcons[5];
                    return RoomType.Rest;
                default:
                    return RoomType.Battle;
            }
        }

        public void SetRooms(GameObject spawnedRoom,int id,int maxNumber,bool sideRoom)
        {
            if (id==1)
            {
                spawnedRoom.AddComponent<BattleRoom>();
                spawnedRoom.GetComponent<BattleRoom>().enemyData = GetComponent<EnemyList>().enemies[0];
                var sRoomComp = spawnedRoom.GetComponent<IRoom>();
                FindChildGameObject(spawnedRoom).GetComponent<SpriteRenderer>().sprite = roomIcons[1];
                sRoomComp.Type = RoomType.Battle;
                sRoomComp.ID = id.ToString();
                //initialize player loc
                GetComponent<Movement>().ChangePlayerLocation(sRoomComp);
                spawnedRoom.name = "Battle Room";
            }
            else if (id==maxNumber)
            {
                spawnedRoom.AddComponent<BattleRoom>();
                var sRoomComp = spawnedRoom.GetComponent<IRoom>();
                FindChildGameObject(spawnedRoom).GetComponent<SpriteRenderer>().sprite = roomIcons[2];
                sRoomComp.Type = RoomType.Battle;
                sRoomComp.ID = id.ToString();
                spawnedRoom.name = "Boss Room";
                //boss room
            }
            else
            {
                
                var sRoomType= RandomizeRoom(spawnedRoom,sideRoom);
                if (sideRoom)
                {
                    remainingSideRoom--;
                }
                else
                {
                    remainingMainRoom--;
                }
                var sRoomComp = spawnedRoom.GetComponent<IRoom>();
                sRoomComp.Type = sRoomType;
                sRoomComp.ID = id.ToString();
                //Change sprite code here.
                //Set different fields for different room types.
                if (sRoomType==RoomType.Battle)
                {
                    //set enemy here
                    spawnedRoom.GetComponent<BattleRoom>().enemyData = GetComponent<EnemyList>().RandomEnemy();
                    spawnedRoom.name = "Battle Room";
                }
                else if (sRoomType==RoomType.Treasure)
                {
                    spawnedRoom.GetComponent<TreasureRoom>().MINValue = 20;
                    spawnedRoom.GetComponent<TreasureRoom>().MAXValue = 60;
                    spawnedRoom.name = "Treasure Room";
                }
                else if (sRoomType == RoomType.Shop)
                {
                    spawnedRoom.name = "Shop Room";
                }
                else if (sRoomType==RoomType.Rest)
                {
                    spawnedRoom.name = "Rest Room";
                }
            }
           
        }

        private GameObject FindChildGameObject(GameObject parent)
        {
            var child = parent;
            Transform objs = parent.GetComponentInChildren<Transform>();
            foreach (Transform obj in objs)
            {
                if (obj.parent!=null)
                {
                    child = obj.gameObject;
                    break;
                }
            }

            return child;
        }

    }
}
