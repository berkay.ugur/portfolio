﻿using System;
using System.Collections.Generic;
using Core;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Dungeon
{
    public class DungeonGenerator : MonoBehaviour
    {
        public GameObject prefabRoom;
        [SerializeField] private int _mainRoomNumber;
        [SerializeField] private int _sideRoomNumber;
        [SerializeField] private List<GameObject> _mainRooms;
        [SerializeField] private List<GameObject> _Rooms;
        public int id = 0;

        public Vector3 _addX=new Vector3(1.5f,0f,0f);
        public Vector3 _addY=new Vector3(0f,1.5f,0f);

        private void Start()
        {
            GenerateDungeon();
        }

        private void GenerateDungeon()
        {
            GenerateMainRooms();
            GenerateSideRooms();
        }

        private void GenerateMainRooms()
        {
            GameObject spawnedRoom;
            GameObject lastSpawnedRoom=null;
            Vector3 currentLocation=Vector3.zero;
            GetComponent<RoomController>().remainingMainRoom = _mainRoomNumber - 2;
           // Debug.Log($"<color=yellow>Main-2{_mainRoomNumber-2}</color>");
            //Debug.Log($"<color=yellow>Get {GetComponent<RoomController>().remainingMainRoom}</color>");
            for (int i = 0; i < _mainRoomNumber; i++)
            {
                spawnedRoom = Instantiate(prefabRoom, currentLocation, quaternion.identity);
                id++;
                //Room randomizer in here
                GetComponent<RoomController>().SetRooms(spawnedRoom,id,_mainRoomNumber,false);
                //GetComponent<RoomController>().remainingMainRoom--;
                //Debug.Log($"W <color=yellow>{GetComponent<RoomController>().remainingMainRoom}</color>");
                //spawnedRoom.AddComponent<BattleRoom>();
                //Set values for every room in here
                _mainRooms.Add(spawnedRoom);
                currentLocation += _addY;
                //link rooms in here
                spawnedRoom.GetComponent<IRoom>().SouthRoom = lastSpawnedRoom;
                if (lastSpawnedRoom!=null) lastSpawnedRoom.GetComponent<IRoom>().NorthRoom = spawnedRoom;
                lastSpawnedRoom = spawnedRoom;
            }
            _Rooms=new List<GameObject>(_mainRooms);
        }

        private void GenerateSideRooms()
        {
            GameObject spawnedRoom;
            GameObject selectedRoom;
            GetComponent<RoomController>().remainingSideRoom = _sideRoomNumber;
            for (int i = 0; i < _sideRoomNumber; i++)
            {
                selectedRoom = _mainRooms[Random.Range(0, _mainRooms.Count-1)];
                var toWest=Random.value<=0.5f;
                if (toWest)
                {
                    while (CheckWestRoom(selectedRoom)!=null)
                    {
                        selectedRoom = CheckWestRoom(selectedRoom);
                    }

                    spawnedRoom = Instantiate(prefabRoom, selectedRoom.transform.position-_addX, Quaternion.identity);
                }
                else
                {
                    while (CheckEastRoom(selectedRoom)!=null)
                    {
                        selectedRoom = CheckEastRoom(selectedRoom);
                    }
                    spawnedRoom = Instantiate(prefabRoom, selectedRoom.transform.position+_addX, Quaternion.identity);
                }

                id++;
                GetComponent<RoomController>().SetRooms(spawnedRoom,id,_sideRoomNumber,true);
                //GetComponent<RoomController>().remainingSideRoom--;
                //spawnedRoom.AddComponent<BattleRoom>();
                if (toWest)
                {
                    spawnedRoom.GetComponent<IRoom>().EastRoom = selectedRoom;
                    selectedRoom.GetComponent<IRoom>().WestRoom = spawnedRoom;
                }
                else
                {
                    spawnedRoom.GetComponent<IRoom>().WestRoom = selectedRoom;
                    selectedRoom.GetComponent<IRoom>().EastRoom = spawnedRoom;
                }
                _Rooms.Add(spawnedRoom);
            }
        }
        private static GameObject CheckWestRoom(GameObject selectedRoom)
        {
            return selectedRoom.GetComponent<IRoom>().WestRoom;
        }

        private static GameObject CheckEastRoom(GameObject selectedRoom)
        {
           return selectedRoom.GetComponent<IRoom>().EastRoom;
        }
    }
}